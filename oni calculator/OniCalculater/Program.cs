﻿using System;
using static System.Net.Mime.MediaTypeNames;

/* A console app in csharp to work as an assistant and 
 * help Oxygen Not Included players in calculating the 
 * amount of fuel they require to achieve a certain 
 * distance and reach a planet based on their rocket configuration!
 * Made by Chitransh Agarwal (GD58)
 */

namespace OniCalculater
{
    class Program                                  
    {

        class Module                                        //class to define different module types
        {
            int weight;                                     //weight of modules

            public Module(int x) { weight = x; }            //constructor to initialise variables
            public int GetWeight() { return weight; }       //func to get weight
        }

        class Engine : Module                               //class to define engine types with their stats. Inherits from module class
        {
            int efficiency;                                 //efficiency of engine

            public Engine(int x, int y) : base(y)           //constructor to initialise variables
            {
                efficiency = x;
            }

            public int GetEfficiency() { return efficiency; }       //func to get efficiency   
        }

        class Booster : Module                                      //class to define booster module. Inherits from module class 
        {
            int distance;                                           //variable to define distance that is added by booster
            public Booster(int x,int y):base(x)                     //constructor to initialise variables
            {
                distance = y;
            }
            
            public int GetDistance() { return distance; }           //func to get distance
        }
        
        static float[] fuelEfficiency = new float[] { 1f, 1.33f };  //array to store efficiency multiplies of different oxygen states

        static Module fuelModule = new Module(100);                 //object to store stats for fuel tanks        
       
        static int totalWeight = 200;                               //variable to store total weight of ship. Initialised with 200 to account for command capsule weight
        static int boosterDistance = 0;                             //variable to store added distance from boosters

        static bool ModuleCalc()                                    //function to calculate weight and distance provided by modules
        {
            int n;
            string ch;

            Console.Clear();
            Console.WriteLine("Select Which Module to Add " +
                "\n1.Cargo(all types) \n2.Research \n3.Booster \n4.None\n");
            string module = Console.ReadLine();
            module = module.ToLower();

            if (module == "none"|| module=="4")                     //returning if no other module is to be added
                return true;
            if (module == "1") module = "cargo";                    //making sure all valid inputs are considered
            else if (module == "2") module = "research";
            else if (module == "3") module = "booster";
            
            switch(module)                                          //switching module for player choices
            {
                case "cargo":                                       //case for cargo modules
                    Module cargo = new Module(2000);                //initialising cargo module with weight 2000

                    Console.Write("\n\nEnter number of modules: ");
                    int.TryParse(Console.ReadLine(), out n);

                    Console.Write("\n\nDo u want to add more modules (y/n): ");
                    ch = Console.ReadLine().ToLower();                              //choice for adding more modules
                    if (ch == "y" || ch == "yes")
                        ModuleCalc();                                               //using recursive function calls to calculate for all modules
                    else
                        totalWeight += cargo.GetWeight() * n;                       //updating weight based on module choices
                    break;

                case "research":                                        //case for research modules
                    Module research = new Module(200);                  //initialising reseach module with weight 200

                    Console.Write("\n\nEnter number of modules: ");
                    int.TryParse(Console.ReadLine(), out n);

                    Console.Write("\n\nDo u want to add more modules (y/n): ");
                    ch = Console.ReadLine().ToLower();
                    if (ch == "y" || ch == "yes")
                        ModuleCalc();
                    else
                        totalWeight += research.GetWeight() * n;
                    break;

                case "booster":                                            //case for booster modules
                    Booster booster = new Booster(1000,12000);             //initialising module with weight 1000 and distance 12000

                    Console.Write("\n\nEnter number of modules: ");
                    int.TryParse(Console.ReadLine(), out n);

                    Console.Write("\n\nDo u want to add more modules (y/n): ");
                    ch = Console.ReadLine().ToLower();
                    if (ch == "y" || ch == "yes")
                        ModuleCalc();
                    else
                    {
                        totalWeight += booster.GetWeight() * n;
                        boosterDistance += booster.GetDistance() * n;               //calculating added raw distance from booster modules
                    }
                    break;
                
                default:
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("\n\nInvalid option selected");               //error message on inputing invalid module type
                    return false;
            }
            return true;
        }

        static void Main(string[] args)
        {
            Console.Title = "Oxygen Not Included Calculator";
            string engine;
            int fuel = 0;
            char ch;
            
            while (true)
            {
                Console.Clear();
                Console.ForegroundColor= ConsoleColor.Yellow;
                Console.WriteLine("\n\t\t\t\tWelcome to Oxygen Not Included Rocket Calculator!\n \n");
                Console.ForegroundColor = ConsoleColor.White;

                Console.WriteLine("Enter the Engine Type from Given Options \n1.steam \n2.petroleum \n3.hydrogen\n");           //options for engine type
                engine = Console.ReadLine();
                engine = engine.ToLower();

                if (engine == "1") engine = "steam";                                //accounting for all valid inputs
                else if (engine == "2") engine = "petroleum";
                else if (engine == "3") engine = "hydrogen";

                if (engine != "steam" && engine != "petroleum" && engine != "hydrogen")     //triggering error for invalid engine type input
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Invalid Engine Type!");                  //error message for invalid engine type
                    goto error;
                }

                Console.Write("\n\nEnter the distance to travel (multiples of 10,000): ");          //input for distance to be travelled
                int.TryParse(Console.ReadLine(), out int distance);
                if (distance % 10000 != 0 || distance>210000 || distance<10000)                //checking if input is a multiple of 10,000 as per the game
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("\n\nInvalid Distance selected!");                        //error message for invalid distance input
                    goto error;
                }

                switch (engine)
                {
                    case "steam":                                                  //case for steam engine
                        Engine steam = new Engine(20, 2000);                       //intialising steam engine with efficiency 20 and weight 2000

                        if (ModuleCalc())                                           //calling ModuleCalc() and checking if any error was triggered
                        {
                            fuel = (distance + totalWeight - boosterDistance) / (steam.GetEfficiency() - 1);        //calculating the ammount of fuel required 
                            if (fuel > 900)                                         //checking if fuel exceeds capacity
                            {
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.WriteLine("\n\nunacheivable distance!");    //error message if fuel exceeds capacity
                                goto error;
                            }
                            Console.ForegroundColor = ConsoleColor.Green;
                            Console.WriteLine("\n\nRequired Fuel = " + fuel);          //output message with required fuel
                            Console.ForegroundColor = ConsoleColor.White;
                        }
                        break;

                    case "petroleum":                                           //case for petroleum engine
                        Engine petroleum = new Engine(40, 200);                 //initialising petroleum engine with efficiency 20 and weight 200
                        if (ModuleCalc())
                        {
                            Console.WriteLine("\n\nwhat fuel type is used :\n1.LOX\n2.Oxylite\n");          //choice for oxygen state
                            string fuelType = Console.ReadLine();
                            fuelType = fuelType.ToLower();
                            if (fuelType == "lox" || fuelType == "1")                                                           //acccounting for all valid inputs
                                fuel = (int)((675 * (distance - boosterDistance + totalWeight + fuelModule.GetWeight())) /
                                    (675 * fuelEfficiency[1] * (petroleum.GetEfficiency() - 1) - fuelModule.GetWeight()));
                            else if (fuelType == "oxylite" || fuelType == "2")
                                fuel = (int)((675 * (distance - boosterDistance + totalWeight + fuelModule.GetWeight())) /
                                    (675 * fuelEfficiency[0] * (petroleum.GetEfficiency() - 1) - fuelModule.GetWeight()));
                            else
                            {
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.WriteLine("\n\ninvalid fuel type!");            //error message for wrong fuel type
                                goto error;
                            }

                            if (fuel > 5400)                                            //error message if fuel exceeds capacity for total 8 fuel tanks(6 fuel+2 oxydiser)
                            {
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.WriteLine("\n\nunacheivable distance!");        //error message if fuel exceeds condition
                                goto error;
                            }
                            Console.ForegroundColor = ConsoleColor.Green;
                            Console.WriteLine("\n\nRequired Fuel = " + fuel);           //output message with required fuel
                            Console.ForegroundColor = ConsoleColor.White;
                        }
                        break;

                    case "hydrogen":                                    //case for hydrogen engine
                        Engine hydrogen = new Engine(60, 500);
                        if (ModuleCalc())
                        {
                            Console.WriteLine("\n\nwhat fuel type is used :\n1.LOX\n2.Oxylite\n");
                            string fuelType = Console.ReadLine();
                            fuelType = fuelType.ToLower();
                            if (fuelType == "lox"|| fuelType=="1")
                                fuel = (int)((675 * (distance - boosterDistance + totalWeight + fuelModule.GetWeight())) /
                                    (675 * fuelEfficiency[1] * (hydrogen.GetEfficiency() - 1) - fuelModule.GetWeight()));
                            else if (fuelType == "oxylite"||fuelType=="2")
                                fuel = (int)((675 * (distance - boosterDistance + totalWeight + fuelModule.GetWeight())) /
                                    (675 * fuelEfficiency[0] * (hydrogen.GetEfficiency() - 1) - fuelModule.GetWeight()));
                            else
                            {
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.WriteLine("\n\ninvalid fuel type!");
                                goto error;
                            }

                            if (fuel > 5400)
                            {
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.WriteLine("\n\nunacheivable distance!");
                                goto error;
                            }
                            Console.ForegroundColor = ConsoleColor.Green;
                            Console.WriteLine("\n\nRequired Fuel = " + fuel);
                            Console.ForegroundColor = ConsoleColor.White;
                        }
                        break;

                    default:                                                             //default case to handle errors
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("\n\nAn error occured!!");
                        goto error;
                }
            error:                                                                      //error handling and asking user if they want to retry or end
                Console.Write("\ndo u want to retry (y/n): ");
                Console.ForegroundColor = ConsoleColor.White;
                ch = Console.ReadLine()[0];
                if (ch == 'n') Environment.Exit(0);                                     //exiting program based on user choice
            }
                
        }
    }
}
