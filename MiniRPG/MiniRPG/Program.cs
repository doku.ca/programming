﻿using System;

//a rpg game where player fights a horde of 5 ogres

namespace MiniRPG
{
    //class to define ogres
    class Ogre
    {
        private int damage;
        protected int hp;
        public int Damage
        {
            get { return damage; }
            set { damage = value; }
        }
        public int Hp
        {
            get { return hp; }
            set { hp = value; }
        }
    }
    //class to define the hero, inherits from ogre
    class Hero : Ogre
    {
        private string name;
        public string Name
        {
            get { return name; }
            set { name = value; }
        }
    }

    
    class Program
    {
        //global variables for the program
        static int baseDamage = 15;
        static int incrementDamage = 3;
        static int baseHP = 50;
        static int incrementHP = 5;
        static int damageTakenHero;
        static int damageTakenOgre;

        //function to calculate damage done each turn
        public static void DamageCalc(int choiceHero,int choiceOgre,int heroDamage,int ogreDamage)
        {
            if (choiceHero == 1 && choiceOgre == 1)
            {
                damageTakenHero = ogreDamage;
                damageTakenOgre = heroDamage;
            }
            else if(choiceHero==1 && choiceOgre==2)
            {
                damageTakenHero = 0;
                damageTakenOgre = heroDamage/2;
            }
            else if(choiceHero==2 && choiceOgre == 1)
            {
                damageTakenHero = ogreDamage / 2;
                damageTakenOgre = 0;
            }
            else
            {
                damageTakenHero = 0;
                damageTakenOgre = 0;
            }
        }

        static void Main(string[] args)
        {
            string name;
            int choiceHero;
            int choiceOgre;
            Random rnd = new Random();
            Ogre[] allOgres = new Ogre[5];
            Hero hero = new Hero();

            //introduction text
            Console.WriteLine("Welcome to the dungeon\n\n");
            Console.WriteLine("Enter Your Name!");
            name = Console.ReadLine();
            Console.Clear();

            hero.Name = name;
            hero.Hp = baseHP + incrementHP;
            hero.Damage = baseDamage;

            //loop to initiate an array of ogres
            for(int i = 0; i < allOgres.Length; i++)
            {
                allOgres[i] = new Ogre();
                allOgres[i].Hp = baseHP + incrementHP * i;
                allOgres[i].Damage = baseDamage + incrementDamage * i;
            }

            Console.WriteLine("A hoard of 5 ogre's have appeared!\n\n");

            //main gameplay loop to iterate over all the ogres
            for(int i = 0; i < allOgres.Length; i++)
            {
                Console.WriteLine("Ogre " + i+1 + " decides to duel you!\n");
                Console.WriteLine("Remember! type 1 to attack and 2 to defend!\n");

                //loop to iterate over a single ogre battle
                while(hero.Hp>=0 && allOgres[i].Hp>= 0)
                {
                    Console.WriteLine("\nMake your move!");
                    int.TryParse(Console.ReadLine(),out choiceHero);
                    choiceOgre =rnd.Next(1,3);

                    if (choiceOgre == 1)
                    {
                        Console.WriteLine("The ogre chose to attack");
                    }
                    else
                    {
                        Console.WriteLine("The ogre chose to defend");
                    }
                    
                    //damage calculation for the turn
                    DamageCalc(choiceHero, choiceOgre,hero.Damage,allOgres[i].Damage);
                    hero.Hp -= damageTakenHero;
                    allOgres[i].Hp -= damageTakenOgre;
                    Console.WriteLine("you took " + damageTakenHero + " damage, the ogre took " + damageTakenOgre);
                    Console.WriteLine("your hp is " + hero.Hp + " and the ogre's hp is " + allOgres[i].Hp);
                    Console.WriteLine("\n\n");
                }

                //condition to check who won the duel
                if (hero.Hp <= 0)
                {
                    Console.WriteLine("Game Over!");
                    Console.ReadLine();
                    System.Environment.Exit(0); 
                }
                else
                {
                    Console.WriteLine("You Defeated the Ogre!");
                    Console.ReadLine();
                    Console.Clear();
                }                   
            }
            Console.WriteLine("Congrats! You won the game!");
            
            Console.ReadLine();
        }
    }
}
